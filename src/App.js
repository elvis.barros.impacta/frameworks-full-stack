import './App.css';
import EVS from './EVS';
import Radiation from './Radiation';
import RickAndMorty from './RickAndMorty';

import { Route, BrowserRouter as Router, Switch, Link } from 'react-router-dom';

function App() {
	return (
		<Router>
			<div className='App'>
				<header className='App-header'>
					<Link to="/">
						Home
					</Link>
					<Link to="/radiation">
						Radiation
					</Link>
					<Link to="/rick-and-morty">
						Rick and Morty (API Client)
					</Link>
				</header>

				<div className="Content">
					<Switch>
						<Route exact path='/'>
							<EVS></EVS>
						</Route>
						<Route path='/radiation'>
							<Radiation></Radiation>
						</Route>
						<Route path='/rick-and-morty'>
							<RickAndMorty></RickAndMorty>
						</Route>
					</Switch>
				</div>
			</div>
		</Router>
	);
}

export default App;
