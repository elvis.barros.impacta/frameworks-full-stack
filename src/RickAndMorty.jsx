import axios from "axios"
import { useState } from "react"

function RickAndMorty() {

    var [personagens, setPersonagens] = useState([])

    const http = axios.create({
        baseURL: 'https://rickandmortyapi.com/api'
    })

    http.get('character').then(response => {
        console.log(response.data.results)
        setPersonagens(response.data.results)
    })


    return (
        <div className="RickAndMorty">
            <h2>
                Rick and Morty
            </h2>

            <ul>
                {personagens.map(personagem => (
                    <div style={{ height: 180, width: 180, display: "inline-block", margin: 6 }}>
                        <img src={ personagem.image } alt="Imagem do personagem" style={{ height: 180, width: 180, display: 'block' }}/>
                        <h6 className="RickAndMorty-nome-personagem">
                            {personagem.name}
                        </h6>
                    </div>
                ))}
            </ul>
        </div>
    )
}

export default RickAndMorty