import radiation from './radiation.png'

function Radiation() {
    return (
        <div>
            <img src={radiation} />
        </div>
    )
}

export default Radiation